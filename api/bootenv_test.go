package api

import (
	"io"
	"os"
	"path"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/rackn/provision/v4/models"
)

func TestBootEnvImport(t *testing.T) {
	fredstage := mustDecode(&models.Stage{}, `
Name: fred
BootEnv: fredhammer
`).(*models.Stage)
	fredhammer := mustDecode(&models.BootEnv{}, `
Available: true
BootParams: Acounted for
Endpoint: ""
Errors:
- Fake error
Initrds:
- stage1.img
Kernel: vmlinuz0
Name: fredhammer
OS:
  IsoFile: sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar
  IsoUrl: http://127.0.0.1:10012/files/sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar
  Name: sledgehammer/708de8b878e3818b1c1bb598a56de968939f9d4b
OptionalParams:
- ntp_servers
- access_keys
Templates:
- Contents: 'Attention all '
  Meta: {}
  Name: pxelinux
  Path: pxelinux.cfg/{{.Machine.HexAddress}}
- Contents: planets of the
  Meta: {}
  Name: elilo
  Path: '{{.Machine.HexAddress}}.conf'
- Contents: Solar Federation
  Meta: {}
  Name: ipxe
  Path: '{{.Machine.Address}}.ipxe'
- Contents: We have assumed control
  Meta: {}
  Name: control.sh
  Path: '{{.Machine.Path}}/control.sh'
Validated: true
`).(*models.BootEnv)
	rt(t,
		"Create fred stage",
		mustDecode(&models.Stage{}, `
Available: false
BootEnv: fredhammer
Description: ""
Endpoint: ""
Errors:
- "fred: BootEnv fredhammer does not exist"
Meta: {}
Name: fred
OptionalParams: []
Profiles: []
ReadOnly: false
Reboot: false
RequiredParams: []
RunnerWait: true
Tasks: []
Templates: []
Validated: true`),
		nil,
		func() (interface{}, error) {
			return fredstage, session.CreateModel(fredstage)
		},
		nil)
	rt(t,
		"Test bootenv install from nonsenical location",
		nil,
		&models.Error{
			Model:    "bootenvs",
			Key:      "",
			Type:     "CLIENT_ERROR",
			Messages: []string{"stat does/not/exist: no such file or directory"},
		},
		func() (interface{}, error) {
			return session.InstallBootEnvFromFile("does/not/exist")
		},
		nil)
	rt(t,
		"Test install from a known to exist directory",
		nil,
		&models.Error{
			Model:    "bootenvs",
			Key:      "",
			Type:     "CLIENT_ERROR",
			Messages: []string{". is a directory.  It needs to be a file."},
		},
		func() (interface{}, error) {
			return session.InstallBootEnvFromFile(".")
		},
		nil)
	rt(t,
		"Test install from a valid path that contains invalid content",
		nil,
		&models.Error{
			Model: "bootenvs",
			Key:   "",
			Type:  "CLIENT_ERROR",
			Messages: []string{
				"error converting YAML to JSON: yaml: line 1: did not find expected node content",
			},
			Code: 0,
		},
		func() (interface{}, error) {
			return session.InstallBootEnvFromFile("test-data/badhammer.yml")
		},
		nil)
	rt(t,
		"Test install from a valid flat install (no ISO)",
		fredhammer,
		nil,
		func() (interface{}, error) {
			res, err := session.InstallBootEnvFromFile("test-data/fredhammer.yml")
			if err != nil {
				return res, err
			}
			res.Errors = []string{"Fake error"}
			return res, err
		},
		nil)
	rt(t,
		"Test ISO upload for valid install (manual with invalid path)",
		nil,
		&models.Error{
			Model:    "isos",
			Key:      "sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar",
			Type:     "DOWNLOAD_NOT_ALLOWED",
			Messages: []string{"Iso not present at server, not present locally, and automatic download forbidden"},
		},
		func() (interface{}, error) {
			env, err := session.GetModel("bootenvs", "fredhammer")
			if err != nil {
				return env, err
			}
			return env, session.InstallISOForBootenv(env.(*models.BootEnv), "/no/iso/here", false)
		},
		nil)
	rt(t,
		"Test ISO upload for valid install (automatic with invalid path and bad source)",
		nil,
		&models.Error{
			Model:    "isos",
			Key:      "http://127.0.0.1:10012/files/sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar",
			Type:     "DOWNLOAD_FAILED",
			Messages: []string{"open /no/iso/here/sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar: no such file or directory"},
		},
		func() (interface{}, error) {
			env, err := session.GetModel("bootenvs", "fredhammer")
			if err != nil {
				return env, err
			}
			return env, session.InstallISOForBootenv(env.(*models.BootEnv), "/no/iso/here", true)
		},
		nil)
	rt(t,
		"Test ISO upload for valid install (valid path, invalid source)",
		nil,
		&models.Error{
			Model:    "isos",
			Key:      "http://127.0.0.1:10012/files/sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar",
			Type:     "DOWNLOAD_FAILED",
			Messages: []string{"Unable to start download of http://127.0.0.1:10012/files/sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar: 404 Not Found"},
		},
		func() (interface{}, error) {
			return nil, session.InstallISOForBootenv(fredhammer, tmpDir, true)
		},
		nil)
	rt(t,
		"Test ISO upload for valid install (valid path, valid source)",
		fredhammer,
		nil,
		func() (interface{}, error) {
			dst, err := os.Create(path.Join(tmpDir, "tftpboot", "files", fredhammer.OS.IsoFile))
			if err != nil {
				return nil, err
			}
			src, err := os.Open(path.Join("../cli/test-data/", fredhammer.OS.IsoFile))
			if err != nil {
				dst.Close()
				return nil, err
			}
			_, err = io.Copy(dst, src)
			dst.Close()
			src.Close()
			if err != nil {
				return nil, err
			}
			err = session.InstallISOForBootenv(fredhammer, tmpDir, true)
			if err != nil {
				return nil, err
			}
			fredhammer.Available = true
			fredhammer.Errors = []string{}
			time.Sleep(15 * time.Second)
			return session.GetModel("bootenvs", fredhammer.Key())
		},
		nil)
	rt(t,
		"Test to make sure fredhammer bootenv is available",
		mustDecode(&models.BootEnv{}, `---
Available: true
BootParams: Acounted for
Description: ""
Documentation: ""
Endpoint: ""
Errors: []
Initrds:
- stage1.img
Kernel: vmlinuz0
Meta: {}
Name: fredhammer
OS:
  Codename: ""
  Family: ""
  IsoFile: sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar
  IsoSha256: ""
  IsoUrl: http://127.0.0.1:10012/files/sledgehammer-708de8b878e3818b1c1bb598a56de968939f9d4b.tar
  Name: sledgehammer/708de8b878e3818b1c1bb598a56de968939f9d4b
  SupportedArchitectures: {}
  Version: ""
OnlyUnknown: false
OptionalParams:
- ntp_servers
- access_keys
ReadOnly: false
RequiredParams: []
Templates:
- Contents: 'Attention all '
  ID: ""
  Meta: {}
  Name: pxelinux
  Path: pxelinux.cfg/{{.Machine.HexAddress}}
- Contents: planets of the
  ID: ""
  Meta: {}
  Name: elilo
  Path: '{{.Machine.HexAddress}}.conf'
- Contents: Solar Federation
  ID: ""
  Meta: {}
  Name: ipxe
  Path: '{{.Machine.Address}}.ipxe'
- Contents: We have assumed control
  ID: ""
  Meta: {}
  Name: control.sh
  Path: '{{.Machine.Path}}/control.sh'
  Validated: true
`),
		nil,
		func() (interface{}, error) {
			err := session.Req().UrlForM(fredhammer).Do(fredhammer)
			return fredhammer, err
		},
		nil)
	rt(t,
		"Test to make sure fred stage is available",
		mustDecode(&models.Stage{}, `
Available: true
BootEnv: fredhammer
Description: ""
Endpoint: ""
Errors: []
Meta: {}
Name: fred
OptionalParams: []
Profiles: []
ReadOnly: false
Reboot: false
RequiredParams: []
RunnerWait: true
Tasks: []
Templates: []
Validated: true`),
		nil,
		func() (interface{}, error) {
			err := session.Req().UrlForM(fredstage).Do(fredstage)
			return fredstage, err
		},
		nil)
	rt(t,
		"Clean up after fredhammer (flat install)",
		nil,
		nil,
		func() (interface{}, error) {
			st, err := session.DeleteModel("stages", "fred")
			if err != nil {
				return st, err
			}
			env, err := session.DeleteModel("bootenvs", "fredhammer")
			if err != nil {
				return env, err
			}
			if err := session.DeleteBlob("isos", env.(*models.BootEnv).OS.IsoFile); err != nil {
				return env, err
			}
			if err := session.DeleteBlob("files", env.(*models.BootEnv).OS.IsoFile); err != nil {
				return env, err
			}
			return nil, nil
		},
		nil)
	rt(t,
		"Install local3 bootenv (flat install)",
		mustDecode(&models.BootEnv{}, `
Available: true
Endpoint: ""
Name: local3
OS:
  Name: local3
Templates:
- ID: local3-pxelinux.tmpl
  Meta: {}
  Name: pxelinux
  Path: pxelinux.cfg/{{.Machine.HexAddress}}
- ID: local3-elilo.tmpl
  Meta: {}
  Name: elilo
  Path: '{{.Machine.HexAddress}}.conf'
- ID: local3-ipxe.tmpl
  Meta: {}
  Name: ipxe
  Path: '{{.Machine.Address}}.ipxe'
Validated: true
`),
		nil,
		func() (interface{}, error) {
			res, err := session.InstallBootEnvFromFile("../cli/test-data/local3.yml")
			if err == nil {
				_, err = session.DeleteModel("bootenvs", "local3")
			}
			if err == nil {
				_, err = session.DeleteModel("templates", "local3-pxelinux.tmpl")
			}
			if err == nil {
				_, err = session.DeleteModel("templates", "local3-elilo.tmpl")
			}
			if err == nil {
				_, err = session.DeleteModel("templates", "local3-ipxe.tmpl")
			}
			return res, err
		},
		nil)
	rt(t,
		"Install local3 bootenv (/bootenvs without /templates)",
		nil,
		&models.Error{
			Model: "bootenvs",
			Key:   "local3",
			Type:  "CLIENT_ERROR",
			Messages: []string{
				"Unable to import template local3-pxelinux.tmpl",
				"Unable to import template local3-elilo.tmpl",
				"Unable to import template local3-ipxe.tmpl",
			},
			Code: 0,
		},
		func() (interface{}, error) {
			benv := path.Join(tmpDir, "bootenvs")
			tgt := path.Join(benv, "local3.yml")
			if err := os.MkdirAll(benv, 0755); err != nil {
				return nil, err
			}
			src, err := filepath.Abs("../cli/test-data/local3.yml")
			if err != nil {
				return nil, err
			}
			os.Symlink(src, tgt)
			res, err := session.InstallBootEnvFromFile(tgt)
			if err == nil {
				_, err = session.DeleteModel("bootenvs", "local3")
			}
			return res, err
		},
		nil)
	rt(t,
		"Install local3 bootenv (/bootenvs and /templates)",
		mustDecode(&models.BootEnv{}, `
Available: true
Name: local3
Endpoint: ""
OS:
  Name: local3
Templates:
- ID: local3-pxelinux.tmpl
  Meta: {}
  Name: pxelinux
  Path: pxelinux.cfg/{{.Machine.HexAddress}}
- ID: local3-elilo.tmpl
  Meta: {}
  Name: elilo
  Path: '{{.Machine.HexAddress}}.conf'
- ID: local3-ipxe.tmpl
  Meta: {}
  Name: ipxe
  Path: '{{.Machine.Address}}.ipxe'
Validated: true
`),
		nil,
		func() (interface{}, error) {
			tmplts := path.Join(tmpDir, "templates")
			if err := os.MkdirAll(tmplts, 0755); err != nil {
				return nil, err
			}
			for _, name := range []string{"local3-pxelinux.tmpl", "local3-elilo.tmpl", "local3-ipxe.tmpl"} {
				tgt := path.Join(tmplts, name)
				src, err := filepath.Abs(path.Join("../cli/test-data", name))
				if err != nil {
					return nil, err
				}
				if err := os.Symlink(src, tgt); err != nil {
					return nil, err
				}
			}
			res, err := session.InstallBootEnvFromFile(path.Join(tmpDir, "bootenvs", "local3.yml"))
			if err == nil {
				_, err = session.DeleteModel("bootenvs", "local3")
			}
			if err == nil {
				_, err = session.DeleteModel("templates", "local3-pxelinux.tmpl")
			}
			if err == nil {
				_, err = session.DeleteModel("templates", "local3-elilo.tmpl")
			}
			if err == nil {
				_, err = session.DeleteModel("templates", "local3-ipxe.tmpl")
			}
			return res, err
		},
		nil)
}
