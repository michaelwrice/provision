// Package api implements a client API for working with
// digitalrebar/provision.
package api

import (
	"encoding/json"
	"fmt"
	"strings"
)

func findEnding(fnString string) int {
	inQuote := false
	inEscape := false
	depth := 0
	for i, r := range fnString {
		if inEscape {
			inEscape = false
			continue
		}
		if r == '\\' {
			inEscape = true
			continue
		}
		if r == '"' {
			inQuote = !inQuote
			continue
		}
		if inQuote {
			continue
		}
		if r == '(' {
			depth++
			continue
		}
		if r == ')' && depth == 0 {
			return i
		}
		if r == ')' {
			depth--
		}
	}
	return -1
}

// ParseAwaitFunctions - parses a string into test functions
// f = And(args)
// f = Or(args)
// f = Not(f)
// f = xx=Eq(value) -- to escape \)
// args = f,args
// args =
func ParseAwaitFunctions(fnString string) ([]TestFunc, error) {
	answer := []TestFunc{}
	remaining := fnString
	for remaining != "" {
		// Skip commas
		if remaining[0] == ',' {
			remaining = remaining[1:]
			continue
		}
		if strings.HasPrefix(remaining, "And(") {
			last := findEnding(remaining[4:])
			if last == -1 {
				return nil, fmt.Errorf("Missing closing paren")
			}
			f, e := ParseAwaitFunctions(remaining[4 : last+4])
			if e != nil {
				return nil, e
			}
			answer = append(answer, AndItems(f...))
			remaining = remaining[4+last+1:]
			continue
		}
		if strings.HasPrefix(remaining, "Or(") {
			last := findEnding(remaining[3:])
			if last == -1 {
				return nil, fmt.Errorf("Missing closing paren")
			}
			f, e := ParseAwaitFunctions(remaining[3 : last+3])
			if e != nil {
				return nil, e
			}
			answer = append(answer, OrItems(f...))
			remaining = remaining[3+last+1:]
			continue
		}
		if strings.HasPrefix(remaining, "Not(") {
			last := findEnding(remaining[4:])
			if last == -1 {
				return nil, fmt.Errorf("Missing closing paren")
			}
			f, e := ParseAwaitFunctions(remaining[4 : last+4])
			if e != nil {
				return nil, e
			}
			if len(f) != 1 {
				return nil, fmt.Errorf("Too many or not enough args for Not")
			}
			answer = append(answer, NotItem(f[0]))
			remaining = remaining[4+last+1:]
			continue
		}

		parts := strings.SplitN(remaining, "=", 2)
		if !strings.HasPrefix(parts[1], "Eq(") {
			return nil, fmt.Errorf("Equals test missing Eq function")
		}

		last := findEnding(parts[1][3:])
		if last == -1 {
			return nil, fmt.Errorf("Missing closing paren")
		}
		svalue := parts[1][3 : last+3]
		var value interface{}
		if err := json.Unmarshal([]byte(svalue), &value); err != nil {
			value = svalue
		}
		answer = append(answer, EqualItem(parts[0], value))
		remaining = remaining[len(parts[0])+1+3+last+1:]
	}

	return answer, nil
}
