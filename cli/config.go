package cli

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
)

const drpcliDir = ".drpcli"

func getConfigDir() string {
	return fmt.Sprintf("%s/%s/", getHome(), drpcliDir)
}

func setConfigLine(key, value string) string {
	return fmt.Sprintf("%s=%s", key, value)
}

func configCommands() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "config",
		Short: "Manage config profiles for drpcli",
	}

	cmd.AddCommand(&cobra.Command{
		Use:          "list",
		Short:        "List available config profiles",
		Args:         cobra.NoArgs,
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			path := getConfigDir()

			files, err := os.ReadDir(path)
			if err != nil || len(files) == 0 {
				return fmt.Errorf("No profiles found under %s", path)
			}

			for i := range files {
				if files[i].IsDir() {
					continue
				}

				name := files[i].Name()
				active := " "

				if defaultProfile == name {
					active = "*"
				}

				fmt.Printf("%s %s\n", active, name)
			}

			return nil
		},
	})

	cmd.AddCommand(&cobra.Command{
		Use:          "remove [name]",
		Short:        "Delete the config profile [name]",
		SilenceUsage: true,
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("remove requires a name argument")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			file := filepath.Join(getConfigDir(), args[0])

			if err := os.Remove(file); err != nil {
				return fmt.Errorf("Error removing %s: %v", file, err)
			}

			fmt.Printf("Removed config %s\n", file)

			return nil
		},
	})

	cmd.AddCommand(&cobra.Command{
		Use:          "save [name]",
		Short:        "Save the current config profile as [name]",
		SilenceUsage: true,
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("save requires a name argument")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			dir := getConfigDir()

			if err := os.MkdirAll(dir, os.ModePerm); err != nil {
				return fmt.Errorf("Error creating %s: %v", dir, err)
			}

			drpclirc := filepath.Join(getHome(), ".drpclirc")
			f, err := os.ReadFile(drpclirc)
			if err != nil && !errors.Is(err, os.ErrNotExist) {
				return fmt.Errorf("Error reading current profile: %v", err)
			}

			lines := []string{
				setConfigLine("RS_PROFILE", args[0]),
			}
			scan := bufio.NewScanner(strings.NewReader(string(f)))

			for scan.Scan() {
				line := scan.Text()
				if strings.HasPrefix(line, "RS_PROFILE") {
					continue
				}

				lines = append(lines, line)
			}

			file := filepath.Join(dir, args[0])
			output := []byte(strings.Join(lines, "\n") + "\n")

			for _, f := range []string{file, drpclirc} {
				if err := os.WriteFile(f, output, os.ModePerm); err != nil {
					return fmt.Errorf("Error saving %s: %v", args[0], err)
				}
			}

			fmt.Printf("Saved config %s\n", file)

			return nil
		},
	})

	cmd.AddCommand(&cobra.Command{
		Use:          "set [field] [value]",
		Short:        "Set [field] to [value] in current config profile",
		SilenceUsage: true,
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("set requires a field and a value")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			home := getHome()
			file := filepath.Join(home, ".drpclirc")

			f, err := os.ReadFile(file)
			if err != nil && errors.Is(err, os.ErrNotExist) {
				return fmt.Errorf("Error updating config %s: %v", file, err)
			}

			lines := []string{}
			scan := bufio.NewScanner(strings.NewReader(string(f)))

			for scan.Scan() {
				if strings.HasPrefix(scan.Text(), args[0]) {
					continue
				}

				lines = append(lines, scan.Text())
			}

			lines = append(lines, setConfigLine(args[0], args[1]))

			output := []byte(strings.Join(lines, "\n") + "\n")

			if err := os.WriteFile(file, output, os.ModePerm); err != nil {
				return fmt.Errorf("Error writing %s: %v", file, err)
			}

			if defaultProfile != "" {
				file = filepath.Join(home, filepath.Join(drpcliDir, defaultProfile))

				if err := os.WriteFile(file, output, os.ModePerm); err != nil {
					return fmt.Errorf("Error writing %s: %v", file, err)
				}
			}

			fmt.Println("Profile updated")

			return nil
		},
	})

	cmd.AddCommand(&cobra.Command{
		Use:          "show [name]",
		Short:        "Show the contents of the current config profile or [name]",
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			file := ".drpclirc"
			if len(args) == 1 {
				file = filepath.Join(".drpcli", args[0])
			}

			file = filepath.Join(getHome(), file)

			b, err := os.ReadFile(file)
			if err != nil {
				return fmt.Errorf("Error reading profile %s: %v", file, err)
			}

			fmt.Print(string(b))

			return nil
		},
	})

	cmd.AddCommand(&cobra.Command{
		Use:          "switch [name]",
		Short:        "Switch the current config profile to [name]",
		SilenceUsage: true,
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("switch requires a name")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			home := getHome()
			src := filepath.Join(home, filepath.Join(".drpcli", args[0]))

			f, err := os.ReadFile(src)
			if err != nil {
				return fmt.Errorf("Error switching profile to %s: %v", args[0], err)
			}

			dst := filepath.Join(home, ".drpclirc")

			if err := os.WriteFile(dst, f, os.ModePerm); err != nil {
				return fmt.Errorf("Error switching profile to %s: %v", args[0], err)
			}

			fmt.Printf("Profile switched to %s\n", args[0])

			return nil
		},
	})

	return cmd
}

func init() {
	addRegistrar(func(c *cobra.Command) { c.AddCommand(configCommands()) })
}
