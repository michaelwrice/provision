package models

import (
	"bytes"
	"encoding/json"
	"log"
)

// UxSetting defines an instance of a UxOption.
//
// swagger:model
type UxSetting struct {
	Validation
	Access
	Meta
	Owned
	Bundled

	// Id is the Name of the object
	Id string `index:",key"`

	// Documentation is an RST string defining the object
	Documentation string
	// Description is a short string description of the object
	Description string
	// Params is an unused residual of the object from previous releases
	Params map[string]interface{}

	// Option is the refrence to the UxOption
	Option string
	// Target is the entity that this applies to user, role, or global
	// user is specified as user++<user name>
	// role is specified as role++<role name>
	Target string
	// Value is the value of the option
	Value string
}

// Key returns the name of the object
func (us *UxSetting) Key() string {
	return us.Id
}

// KeyName returns the Name field of the Object
func (us *UxSetting) KeyName() string {
	return "Id"
}

// AuthKey returns the field of the Object to use for Auth
func (us *UxSetting) AuthKey() string {
	return us.Key()
}

// Prefex returns the type of object
func (us *UxSetting) Prefix() string {
	return "ux_settings"
}

// GetDocumentaiton returns the object's Documentation
func (us *UxSetting) GetDocumentation() string {
	return us.Documentation
}

// GetDescription returns the object's Description
func (us *UxSetting) GetDescription() string {
	return us.Description
}

// Clone the UxSetting
func (us *UxSetting) Clone() *UxSetting {
	ci2 := &UxSetting{}
	buf := bytes.Buffer{}
	enc, dec := json.NewEncoder(&buf), json.NewDecoder(&buf)
	if err := enc.Encode(us); err != nil {
		log.Panicf("Failed to encode endpoint:%s: %v", us.Id, err)
	}
	if err := dec.Decode(ci2); err != nil {
		log.Panicf("Failed to decode endpoint:%s: %v", us.Id, err)
	}
	return ci2
}

// Fill initializes and empty object
func (us *UxSetting) Fill() {
	us.Validation.fill(us)
	if us.Meta == nil {
		us.Meta = Meta{}
	}
	if us.Errors == nil {
		us.Errors = []string{}
	}
	if us.Params == nil {
		us.Params = map[string]interface{}{}
	}
}

// SliceOf returns a slice of objects
func (us *UxSetting) SliceOf() interface{} {
	s := []*UxSetting{}
	return &s
}

// ToModels converts a Slice of objects into a list of Model
func (us *UxSetting) ToModels(obj interface{}) []Model {
	items := obj.(*[]*UxSetting)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// CanHaveActions says that actions can be added to this object type
func (us *UxSetting) CanHaveActions() bool {
	return true
}

// SetName sets the name. In this case, it sets Id.
func (us *UxSetting) SetName(name string) {
	us.Id = name
}
