package models

// IdentityProvider tracks SAML identity providers
//
// swagger:model
type IdentityProvider struct {
	Validation
	Access
	Meta
	Owned
	Bundled
	// Name is the name of this identity provider
	//
	// required: true
	Name string
	// A description of this Identity Provider.  This should tell what it is for,
	// any special considerations that should be taken into account when
	// using it, etc.
	Description string
	// Documentation of this Identity Provider.  This should tell what
	// the identity provider is for, any special considerations that
	// should be taken into account when using it, etc. in rich structured text (rst).
	Documentation string
	// MetaDataUrl - URL to get the metadata for this IdP - instead of MetaDataBlob
	MetaDataUrl string
	// MetaDataBlob - String form of the metadata - instead of MetaDataUrl
	MetaDataBlob string
	// UserAttribute - specifies the attribute in the Assertions to use as username
	UserAttribute string
	// GroupAttribute - specifies the attribute in the Assertions to use as group memberships
	GroupAttribute string
	// GroupToRoles - defines the group names that map to DRP Roles
	GroupToRoles map[string][]string
	// DefaultRole - defines the default role to give these users
	DefaultRole string
	// DenyIfNoGroups - defines if the auth should fail if no groups are found in the GroupAttribute
	DenyIfNoGroups bool
	// DisplayName - The name to display to user
	DisplayName string
	// LogoPath - The path on DRP or the URL to the logo icon
	LogoPath string
}

func (ip *IdentityProvider) GetMeta() Meta {
	return ip.Meta
}

func (ip *IdentityProvider) SetMeta(d Meta) {
	ip.Meta = d
}

// GetDocumentation returns the object's documentation
func (ip *IdentityProvider) GetDocumentation() string {
	return ip.Documentation
}

// GetDescription returns the object's description
func (ip *IdentityProvider) GetDescription() string {
	return ip.Description
}

func (ip *IdentityProvider) Prefix() string {
	return "identity_providers"
}

func (ip *IdentityProvider) Key() string {
	return ip.Name
}

func (ip *IdentityProvider) KeyName() string {
	return "Name"
}

func (ip *IdentityProvider) Fill() {
	ip.Validation.fill(ip)
	if ip.Meta == nil {
		ip.Meta = Meta{}
	}
	if ip.GroupToRoles == nil {
		ip.GroupToRoles = map[string][]string{}
	}
}

// Validate makes sure that the object is valid (outside of references)
func (ip *IdentityProvider) Validate() {
	ip.AddError(ValidName("Invalid Name", ip.Name))
}

func (ip *IdentityProvider) AuthKey() string {
	return ip.Key()
}

func (ip *IdentityProvider) SliceOf() interface{} {
	s := []*IdentityProvider{}
	return &s
}

func (ip *IdentityProvider) ToModels(obj interface{}) []Model {
	items := obj.(*[]*IdentityProvider)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

func (ip *IdentityProvider) CanHaveActions() bool {
	return true
}
