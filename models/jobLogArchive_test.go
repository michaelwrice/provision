package models

import (
	"bytes"
	"context"
	"github.com/pborman/uuid"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"sort"
	"testing"
	"time"
)

func TestJobLogArchive(t *testing.T) {
	tmp := t.TempDir()
	t.Logf("Testing archive processing of current directory")
	name := path.Join(tmp, "archive.logs")
	archive, err := OpenJobLogArchive(name, nil, nil)
	if err != nil {
		t.Fatalf("Error creating job log archive: %v", err)
	}
	defer archive.Close()
	paths, err := filepath.Glob("*.go")
	if err != nil {
		t.Fatalf("Error fetching paths: %v", err)
	}
	if len(paths) == 0 {
		t.Fatalf("No Go files")
	}
	machineID := uuid.NewRandom().Array()
	ids := make([]uuid.Array, len(paths))
	for i := range paths {
		ids[i] = NewUUID6().Array()
		fi, err := os.Open(paths[i])
		if err != nil {
			t.Fatalf("Failed to open %s: %v", paths[i], err)
		}
		t.Logf("Adding %s as id %s", paths[i], ids[i])
		func() {
			defer fi.Close()
			if err := archive.ImportFile(fi, ids[i], machineID); err != nil {
				t.Fatalf("Failed to import %s as %s: %v", paths[i], ids[i], err)
			}
		}()
	}
	t.Logf("Checking that there are entries for everything we added")
	entries, err := archive.Entries()
	if err != nil {
		t.Fatalf("Failed to fetch archive entries: %v", err)
	}
	sort.Slice(entries, func(i, j int) bool { return bytes.Compare(entries[i].JobID[:], entries[j].JobID[:]) == -1 })
	if len(entries) != len(ids) {
		t.Fatalf("%d paths, but %d stored entries", len(paths), len(entries))
	}
	for i := range entries {
		if !bytes.Equal(ids[i][:], entries[i].JobID[:]) {
			t.Fatalf("Mismatched ID: %s != %s", ids[i], entries[i].JobID)
		}
	}
	t.Logf("Checking that all log entries were stored properly")
	for i := range ids {
		logEntry, err := archive.LogFor(ids[i])
		if err != nil {
			t.Fatalf("Missing log entry for %s (id %s): %v", paths[i], ids[i], err)
		}
		func() {
			defer logEntry.Close()
			fi, err := os.Open(paths[i])
			if err != nil {
				t.Fatalf("Failed to open %s: %v", paths[i], err)
			}
			defer fi.Close()
			mta := &ModTimeSha{}
			if _, err := mta.Regenerate(fi); err != nil {
				t.Fatalf("Error fetching MTA for %s (id %s): %v", paths[i], ids[i], err)
			}
			if !mta.ModTime.Equal(logEntry.ModTime) {
				t.Fatalf("Mismatched mod times")
			}
			if !bytes.Equal(mta.ShaSum, logEntry.ShaSum[:]) {
				t.Fatalf("Mismatched shasums")
			}
			buf1, err := ioutil.ReadAll(logEntry)
			if err != nil {
				t.Fatalf("Failed to read log entry for %s (id %s): %v", paths[i], ids[i], err)
			}
			buf2, err := ioutil.ReadAll(fi)
			if err != nil {
				t.Fatalf("Failed to read file for %s (id %s): %v", paths[i], ids[i], err)
			}
			if !bytes.Equal(buf1, buf2) {
				t.Fatalf("Saved log does not match source file")
			}
		}()
	}
	oldst, err := os.Stat(archive.name)
	if err != nil {
		t.Fatalf("Error stat'ing %s: %v", archive.name, err)
	}
	t.Logf("All entries stored and verified.")
	t.Logf("Waiting to make sure backing file is closed")
	time.Sleep(10 * time.Second)
	t.Logf("Re-checking entries")
	entries, err = archive.Entries()
	if err != nil {
		t.Fatalf("Failed to fetch archive entries: %v", err)
	}
	sort.Slice(entries, func(i, j int) bool { return bytes.Compare(entries[i].JobID[:], entries[j].JobID[:]) == -1 })
	if len(entries) != len(ids) {
		t.Fatalf("%d paths, but %d stored entries", len(paths), len(entries))
	}
	for i := range entries {
		if !bytes.Equal(ids[i][:], entries[i].JobID[:]) {
			t.Fatalf("Mismatched ID: %s != %s", ids[i], entries[i].JobID)
		}
	}
	t.Logf("Removing entries from the archive")
	for i := range ids {
		if err = archive.Remove(ids[i]); err != nil {
			t.Fatalf("Failed to remove %s (id %s): %v", paths[i], ids[i], err)
		}
	}
	t.Logf("Waiting for file to be closed.")
	time.Sleep(10 * time.Second)
	newSt, err := os.Stat(archive.name)
	if err != nil {
		t.Fatalf("Missing stat info for %s: %v", archive.name, err)
	}
	if oldst.Size() <= newSt.Size() {
		t.Fatalf("Archive did not shrink after removing everything")
	}
	if newSt.Size() != jleSize<<1 {
		t.Fatalf("New log size %d instead of %d", newSt.Size(), jleSize<<1)
	}
	t.Logf("Archive shrank from %d to %d bytes", oldst.Size(), newSt.Size())
	t.Logf("Batch adding files")
	if err = archive.ImportFiles(machineID, ids, paths); err != nil {
		t.Fatalf("Failed to import files: %v", err)
	}
	t.Logf("Checking that there are entries for everything we added")
	entries, err = archive.Entries()
	if err != nil {
		t.Fatalf("Failed to fetch archive entries: %v", err)
	}
	sort.Slice(entries, func(i, j int) bool { return bytes.Compare(entries[i].JobID[:], entries[j].JobID[:]) == -1 })
	if len(entries) != len(ids) {
		t.Fatalf("%d paths, but %d stored entries", len(paths), len(entries))
	}
	for i := range entries {
		if !bytes.Equal(ids[i][:], entries[i].JobID[:]) {
			t.Fatalf("Mismatched ID: %s != %s", ids[i], entries[i].JobID)
		}
	}
	t.Logf("Checking that all log entries were stored properly")
	for i := range ids {
		logEntry, err := archive.LogFor(ids[i])
		if err != nil {
			t.Fatalf("Missing log entry for %s (id %s): %v", paths[i], ids[i], err)
		}
		func() {
			defer logEntry.Close()
			fi, err := os.Open(paths[i])
			if err != nil {
				t.Fatalf("Failed to open %s: %v", paths[i], err)
			}
			defer fi.Close()
			mta := &ModTimeSha{}
			if _, err := mta.Regenerate(fi); err != nil {
				t.Fatalf("Error fetching MTA for %s (id %s): %v", paths[i], ids[i], err)
			}
			if !mta.ModTime.Equal(logEntry.ModTime) {
				t.Fatalf("Mismatched mod times")
			}
			if !bytes.Equal(mta.ShaSum, logEntry.ShaSum[:]) {
				t.Fatalf("Mismatched shasums")
			}
			buf1, err := ioutil.ReadAll(logEntry)
			if err != nil {
				t.Fatalf("Failed to read log entry for %s (id %s): %v", paths[i], ids[i], err)
			}
			buf2, err := ioutil.ReadAll(fi)
			if err != nil {
				t.Fatalf("Failed to read file for %s (id %s): %v", paths[i], ids[i], err)
			}
			if !bytes.Equal(buf1, buf2) {
				t.Fatalf("Saved log does not match source file")
			}
		}()
	}
	t.Logf("All entries stored and verified.")
	t.Logf("Waiting to make sure backing file is closed")
	time.Sleep(10 * time.Second)
	t.Logf("Re-checking entries")
	entries, err = archive.Entries()
	if err != nil {
		t.Fatalf("Failed to fetch archive entries: %v", err)
	}
	sort.Slice(entries, func(i, j int) bool { return bytes.Compare(entries[i].JobID[:], entries[j].JobID[:]) == -1 })
	if len(entries) != len(ids) {
		t.Fatalf("%d paths, but %d stored entries", len(paths), len(entries))
	}
	for i := range entries {
		if !bytes.Equal(ids[i][:], entries[i].JobID[:]) {
			t.Fatalf("Mismatched ID: %s != %s", ids[i], entries[i].JobID)
		}
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	iter, err := archive.Iter(ctx)
	if err != nil {
		t.Fatalf("Error opening iterator: %v", err)
	}
	for i := 0; ; i++ {
		entry := iter()
		if entry == nil {
			break
		}
		func() {
			defer entry.Close()
			fi, err := os.Open(paths[i])
			if err != nil {
				t.Fatalf("Failed to open %s: %v", paths[i], err)
			}
			defer fi.Close()
			mta := &ModTimeSha{}
			if _, err := mta.Regenerate(fi); err != nil {
				t.Fatalf("Error fetching MTA for %s (id %s): %v", paths[i], ids[i], err)
			}
			if !mta.ModTime.Equal(entry.ModTime) {
				t.Fatalf("Mismatched mod times")
			}
			if !bytes.Equal(mta.ShaSum, entry.ShaSum[:]) {
				t.Fatalf("Mismatched shasums")
			}
			buf1, err := ioutil.ReadAll(entry)
			if err != nil {
				t.Fatalf("Failed to read log entry for %s (id %s): %v", paths[i], ids[i], err)
			}
			buf2, err := ioutil.ReadAll(fi)
			if err != nil {
				t.Fatalf("Failed to read file for %s (id %s): %v", paths[i], ids[i], err)
			}
			if !bytes.Equal(buf1, buf2) {
				t.Fatalf("Saved log does not match source file")
			}
		}()
	}
}
