package models

import (
	"time"
)

type Connection struct {
	Type       string
	RemoteAddr string
	Principal  string
	CreateTime time.Time
}

type Connections map[string]Connection
type ConnectionsList []Connection
