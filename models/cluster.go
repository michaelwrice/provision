package models

import (
	"net"
	"reflect"
	"strings"

	"github.com/pborman/uuid"
)

// Cluster represents a single bare-metal system that the provisioner
// should manage the boot environment for.
// swagger:model
type Cluster struct {
	Validation
	Access
	Meta
	Owned
	Bundled
	Partialed
	TaskState
	MachineBase
}

func (n *Cluster) IsLocked() bool {
	return n.Locked
}

func (n *Cluster) GetMeta() Meta {
	return n.Meta
}

func (n *Cluster) SetMeta(d Meta) {
	n.Meta = d
}

func (n *Cluster) Validate() {
	if arch, ok := SupportedArch(n.Arch); !ok {
		n.Errorf("Unsupported arch %s", n.Arch)
	} else if arch != n.Arch {
		n.Errorf("Please use %s for Arch instead of %s", arch, n.Arch)
	}
	n.AddError(ValidMachineName("Invalid Name", n.Name))
	n.AddError(ValidName("Invalid Stage", n.Stage))
	n.AddError(ValidName("Invalid BootEnv", n.BootEnv))
	if n.Workflow != "" {
		n.AddError(ValidName("Invalid Workflow", n.Workflow))
	}
	for _, p := range n.Profiles {
		n.AddError(ValidNumberName("Invalid Profile", p))
	}
	for _, t := range n.Tasks {
		parts := strings.SplitN(t, ":", 2)
		if len(parts) == 2 {
			switch parts[0] {
			case "stage":
				n.AddError(ValidName("Invalid Stage", parts[1]))
			case "bootenv":
				n.AddError(ValidName("Invalid BootEnv", parts[1]))
			case "chroot", "context":
			case "action":
				pparts := strings.SplitN(parts[1], ":", 2)
				if len(pparts) == 2 {
					n.AddError(ValidName("Invalid Plugin", pparts[0]))
					n.AddError(ValidName("Invalid Action", pparts[1]))
				} else {
					n.AddError(ValidName("Invalid Action", parts[1]))
				}
			}
		} else {
			n.AddError(ValidName("Invalid Task", t))
		}
	}
	for _, m := range n.HardwareAddrs {
		if _, err := net.ParseMAC(m); err != nil {
			n.Errorf("Invalid Hardware Address `%s`: %v", m, err)
		}
	}
}

func (n *Cluster) UUID() string {
	return n.Uuid.String()
}

func (n *Cluster) Prefix() string {
	return "clusters"
}

func (n *Cluster) Key() string {
	return n.UUID()
}

func (n *Cluster) KeyName() string {
	return "Uuid"
}

func (n *Cluster) GetDescription() string {
	return n.Description
}

func (n *Cluster) Fill() {
	if n.Meta == nil {
		n.Meta = Meta{}
	}
	n.Validation.fill(n)
	if n.Profiles == nil {
		n.Profiles = []string{}
	}
	if n.Tasks == nil {
		n.Tasks = []string{}
	}
	if n.Params == nil {
		n.Params = map[string]interface{}{}
	}
	if n.HardwareAddrs == nil {
		n.HardwareAddrs = []string{}
	}
	if n.TaskErrorStacks == nil {
		n.TaskErrorStacks = []*TaskStack{}
	}
	if n.Arch == "" {
		n.Arch = "amd64"
	}
	(&n.Fingerprint).Fill()
}

func (n *Cluster) AuthKey() string {
	return n.Key()
}

func (n *Cluster) SliceOf() interface{} {
	s := []*Cluster{}
	return &s
}

func (n *Cluster) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Cluster)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// match Param interface
func (n *Cluster) GetParams() map[string]interface{} {
	return copyMap(n.Params)
}

func (n *Cluster) SetParams(p map[string]interface{}) {
	n.Params = copyMap(p)
}

// match Profiler interface
func (n *Cluster) GetProfiles() []string {
	return n.Profiles
}

func (n *Cluster) SetProfiles(p []string) {
	n.Profiles = p
}

// match BootEnver interface
func (n *Cluster) GetBootEnv() string {
	return n.BootEnv
}

func (n *Cluster) SetBootEnv(s string) {
	n.BootEnv = s
}

// match TaskRunner interface

// GetTasks returns the task list
func (n *Cluster) GetTasks() []string {
	return n.Tasks
}

// SetTasks sets the task list
func (n *Cluster) SetTasks(t []string) {
	n.Tasks = t
}

// RunningTask returns the current task index
func (n *Cluster) RunningTask() int {
	return n.CurrentTask
}

// SetName sets the name
func (n *Cluster) SetName(name string) {
	n.Name = name
}

// SplitTasks slits the machine's Tasks list into 3 subsets:
//
// 1. the immutable past, which cannot be chnaged by task list modification
//
// 2. The mutable present, which contains tasks that can be deleted, and where tasks can be added.
//
// 3. The immutable future, which also cannot be changed.
func (n *Cluster) SplitTasks() (thePast []string, thePresent []string, theFuture []string) {
	thePast, thePresent, theFuture = []string{}, []string{}, []string{}
	if len(n.Tasks) == 0 {
		return
	}
	if n.CurrentTask == -1 {
		thePresent = n.Tasks[:]
	} else if n.CurrentTask >= len(n.Tasks) {
		thePast = n.Tasks[:]
	} else {
		thePast = n.Tasks[:n.CurrentTask+1]
		thePresent = n.Tasks[n.CurrentTask+1:]
	}
	for i := 0; i < len(thePresent); i++ {
		if strings.HasPrefix(thePresent[i], "stage:") {
			theFuture = thePresent[i:]
			thePresent = thePresent[:i]
			break
		}
	}
	return
}

// AddTasks is a helper for adding tasks to the machine Tasks list in
// the mutable present.
func (n *Cluster) AddTasks(offset int, tasks ...string) error {
	thePast, thePresent, theFuture := n.SplitTasks()
	if offset < 0 {
		offset += len(thePresent) + 1
		if offset < 0 {
			offset = len(thePresent)
		}
	}
	if offset >= len(thePresent) {
		offset = len(thePresent)
	}
	if offset == 0 {
		if len(thePresent) >= (len(tasks)+offset) &&
			reflect.DeepEqual(tasks, thePresent[offset:offset+len(tasks)]) {
			// We are already in the desired task state.
			return nil
		}
		thePresent = append(tasks, thePresent...)
	} else if offset == len(thePresent) {
		if len(thePresent) >= len(tasks) &&
			reflect.DeepEqual(tasks, thePresent[len(thePresent)-len(tasks):]) {
			// We are alredy in the desired state
			return nil
		}
		thePresent = append(thePresent, tasks...)
	} else {
		if len(thePresent[offset:]) >= len(tasks) &&
			reflect.DeepEqual(tasks, thePresent[offset:offset+len(tasks)]) {
			// Already in the desired state
			return nil
		}
		res := []string{}
		res = append(res, thePresent[:offset]...)
		res = append(res, tasks...)
		res = append(res, thePresent[offset:]...)
		thePresent = res
	}
	thePresent = append(thePresent, theFuture...)
	n.Tasks = append(thePast, thePresent...)
	return nil
}

// DelTasks allows you to delete tasks in the mutable present.
func (n *Cluster) DelTasks(tasks ...string) {
	if len(tasks) == 0 {
		return
	}
	thePast, thePresent, theFuture := n.SplitTasks()
	if len(thePresent) == 0 {
		return
	}
	nextThePresent := []string{}
	i := 0
	for _, c := range thePresent {
		if i < len(tasks) && tasks[i] == c {
			i++
		} else {
			nextThePresent = append(nextThePresent, c)
		}
	}
	nextThePresent = append(nextThePresent, theFuture...)
	n.Tasks = append(thePast, nextThePresent...)
}

// CanHaveActions returns that the object can have actions
func (n *Cluster) CanHaveActions() bool {
	return true
}

// GetName gets the name
func (n *Cluster) GetName() string {
	return n.Name
}

// GetStage gets the name
func (n *Cluster) GetStage() string {
	return n.Stage
}

// GetWorkflow gets the name
func (n *Cluster) GetWorkflow() string {
	return n.Workflow
}

// GetCurrentJob gets the name
func (n *Cluster) GetCurrentJob() uuid.UUID {
	return n.CurrentJob
}

// GetContext gets the name
func (n *Cluster) GetContext() string {
	return n.Context
}

// SetStage sets the name
func (n *Cluster) SetStage(s string) {
	n.Stage = s
}

// GetRunnable sets runnable
func (n *Cluster) GetRunnable() bool {
	return n.Runnable
}

// SetRunnable sets runnable
func (n *Cluster) SetRunnable(b bool) {
	n.Runnable = b
}

// GetState returns fake state
func (n *Cluster) GetState() string {
	return "created"
}

// GetWorkOrderMode returns the current work order mode
func (n *Cluster) GetWorkOrderMode() bool {
	return n.WorkOrderMode
}

// SetWorkOrderMode sets the work order mode
func (n *Cluster) SetWorkOrderMode(v bool) {
	n.WorkOrderMode = v
}
