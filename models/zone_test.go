package models

import (
	"testing"

	"github.com/miekg/dns"
)

func TestZoneDnsRecords(t *testing.T) {
	z := &Zone{}
	for key, fun := range dns.TypeToRR {
		if _, ok := DnsIgnoreTypes[key]; ok {
			continue
		}
		rr := fun()
		rr.Header().Rrtype = key
		if err := z.AddRecordFromRR(rr); err != nil {
			t.Errorf("RR to V: %d -> %v\n", key, err)
		}
	}
	z.addRecord("A", 0, "eval:{{.Machine.Name}}", []string{"{{.Machine.Address}}"})
	z.Name = "testZone"
	z.Origin = "test.Zone."
	z.Validate()
	// if !z.Validated || !z.Available {
	// t.Errorf("Validated: %v, Available: %v, Errors: %v\n", z.Validated, z.Available, z.Errors)
	// }
	for _, rec := range z.Records {
		qtype := dns.StringToType[string(rec.Type)]
		rr := ZoneValuesToRec(string(rec.Name), qtype, 0, rec.TTL, rec.Value)
		if rr == nil {
			t.Errorf("V to RR: %s -> %v\n", rec.Type, rr)
		}
	}
}
