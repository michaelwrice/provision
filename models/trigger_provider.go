package models

// TriggerProvider defines a type of trigger and its required and optional
// parameters.  This is usually provided by plugins, but system versions exist.
//
// swagger:model
type TriggerProvider struct {
	Validation
	Access
	Meta
	Owned
	Bundled
	// Name is the key of this particular TriggerProvider.
	// required: true
	Name string `index:",key"`
	// Description is a one-line description of the parameter.
	Description string
	// Documentation details what the parameter does, what values it can
	// take, what it is used for, etc.
	Documentation string
	// Profiles to apply to this machine in order when looking
	// for a parameter during rendering.
	Profiles []string
	// Params that have been directly set on the TriggerProvider.
	Params map[string]interface{}
	// Method defines the method used on that URL
	Method string
	// RequiredParameters define the values that must be in Params on the Trigger
	RequiredParameters []string
	// OptionalParameters define the optional values that can be in Params on the Trigger
	OptionalParameters []string
	// NoURL indicates that a URL should NOT be created
	NoURL bool
}

func (r *TriggerProvider) GetMeta() Meta {
	return r.Meta
}

func (r *TriggerProvider) SetMeta(d Meta) {
	r.Meta = d
}

// GetDocumentaiton returns the object's Documentation
func (r *TriggerProvider) GetDocumentation() string {
	return r.Documentation
}

// GetDescription returns the object's Description
func (r *TriggerProvider) GetDescription() string {
	return r.Description
}

func (r *TriggerProvider) Validate() {
	r.AddError(ValidName("Invalid TriggerProvider Name", r.Name))
}

func (r *TriggerProvider) Prefix() string {
	return "trigger_providers"
}

func (r *TriggerProvider) Key() string {
	return r.Name
}

func (r *TriggerProvider) KeyName() string {
	return "Name"
}

func (r *TriggerProvider) Fill() {
	if r.Meta == nil {
		r.Meta = Meta{}
	}
	if r.Profiles == nil {
		r.Profiles = []string{}
	}
	if r.Params == nil {
		r.Params = map[string]interface{}{}
	}
	if r.RequiredParameters == nil {
		r.RequiredParameters = []string{}
	}
	if r.OptionalParameters == nil {
		r.OptionalParameters = []string{}
	}
	r.Validation.fill(r)
}

func (r *TriggerProvider) AuthKey() string {
	return r.Key()
}

func (r *TriggerProvider) SliceOf() interface{} {
	s := []*TriggerProvider{}
	return &s
}

func (r *TriggerProvider) ToModels(obj interface{}) []Model {
	items := obj.(*[]*TriggerProvider)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

func (r *TriggerProvider) CanHaveActions() bool {
	return true
}

// match Profiler interface

// GetProfiles gets the profiles on this stage
func (r *TriggerProvider) GetProfiles() []string {
	return r.Profiles
}

// SetProfiles sets the profiles on this stage
func (r *TriggerProvider) SetProfiles(p []string) {
	r.Profiles = p
}

// match Paramer interface

// GetParams gets the parameters on this stage
func (r *TriggerProvider) GetParams() map[string]interface{} {
	return copyMap(r.Params)
}

// SetParams sets the parameters on this stage
func (r *TriggerProvider) SetParams(p map[string]interface{}) {
	r.Params = copyMap(p)
}

// SetName sets the name of the object
func (r *TriggerProvider) SetName(n string) {
	r.Name = n
}
