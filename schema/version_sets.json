{
  "$defs": {
    "Element": {
      "additionalProperties": false,
      "description": "Element define a part of the endpoint This can be a file, a pref, global profile parameter, DRP itself, content packages, plugin_providers, or plugins.",
      "properties": {
        "ActualVersion": {
          "description": "ActualVersion is the actual catalog version referenced by this element.\nThis is used for translating tip and stable into a real version.\nThis is the source of the file element.  This can be a relative or absolute path or an URL.",
          "type": "string"
        },
        "Name": {
          "description": "Name defines the name of the element.  Normally, this is the\nname of the the DRP, DRPUX, filename, plugin, ContentPackage, or PluginProvider Name.\nFor Global and Pref, these are the name of the global parameter or preference.",
          "type": "string"
        },
        "Type": {
          "description": "Type defines the type of element\nThis can be:\n  DRP, DRPUX, File, Global, Plugin, Pref, PluginProvider, ContentPackage",
          "type": "string"
        },
        "Version": {
          "description": "Version defines the short or reference version of the element.\ne.g. tip, stable, v4.3.6",
          "type": "string"
        }
      },
      "type": "object"
    },
    "FileData": {
      "additionalProperties": false,
      "properties": {
        "Explode": {
          "type": "boolean"
        },
        "Path": {
          "type": "string"
        },
        "Sha256Sum": {
          "type": "string"
        },
        "Source": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "Plugin": {
      "additionalProperties": false,
      "description": "Plugin represents a single instance of a running plugin.",
      "properties": {
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Description": {
          "description": "A description of this plugin.  This can contain any reference\ninformation for humans you want associated with the plugin.",
          "type": "string"
        },
        "Documentation": {
          "description": "Documentation of this plugin.  This should tell what\nthe plugin is for, any special considerations that\nshould be taken into account when using it, etc. in rich structured text (rst).",
          "type": "string"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "Name": {
          "description": "The name of the plugin instance.  THis must be unique across all\nplugins.\n\nrequired: true",
          "type": "string"
        },
        "Params": {
          "additionalProperties": true,
          "description": "Any additional parameters that may be needed to configure\nthe plugin.",
          "type": "object"
        },
        "Partial": {
          "description": "Partial tracks if the object is not complete when returned.\nread only: true",
          "type": "boolean"
        },
        "PluginErrors": {
          "description": "Error unrelated to the object validity, but the execution\nof the plugin.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Provider": {
          "description": "The plugin provider for this plugin\n\nrequired: true",
          "type": "string"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        }
      },
      "type": "object"
    },
    "VersionSet": {
      "additionalProperties": false,
      "description": "VersionSet structure that handles RawModel instead of dealing with RawModel which is how DRP is storing it.",
      "properties": {
        "Apply": {
          "type": "boolean"
        },
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Components": {
          "items": {
            "$ref": "#/$defs/Element"
          },
          "type": "array"
        },
        "DRPUXVersion": {
          "type": "string"
        },
        "DRPVersion": {
          "type": "string"
        },
        "Description": {
          "type": "string"
        },
        "Documentation": {
          "type": "string"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Files": {
          "items": {
            "$ref": "#/$defs/FileData"
          },
          "type": "array"
        },
        "Global": {
          "type": "object"
        },
        "Id": {
          "type": "string"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "Plugins": {
          "items": {
            "$ref": "#/$defs/Plugin"
          },
          "type": "array"
        },
        "Prefs": {
          "patternProperties": {
            ".*": {
              "type": "string"
            }
          },
          "type": "object"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        }
      },
      "type": "object"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/version-set",
  "$ref": "#/$defs/VersionSet",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}