#Google Quickstart
variable "drp_username" {
  type    = string
  default = "rocketskates"
}

variable "drp_password" {
  type    = string
  default = "r0cketsk8ts"
}

variable "drp_id" {
  type    = string
  default = "!default!"
}

variable "drp_version" {
  type    = string
  default = "tip"
}

variable "gcp_project" {
  type = string
}

variable "gcp_region" {
  type    = string
  default = "us-central1"
}

variable "gcp_zone" {
  type    = string
  default = "us-central1-a"
}

# Configure the Google provider
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.26.0"
    }
  }
}
provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
  zone    = var.gcp_zone
}

locals {
  drp_id            = var.drp_id != "!default!" ? "--drp-id=${var.drp_id}" : ""
  cloud_init_script = <<-EOT
  #!/usr/bin/env bash
  value=$(curl -sfL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)
  curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --version=${var.drp_version} --ipaddr=$value --drp-password=${var.drp_password} --remove-rocketskates --drp-user=${var.drp_username} ${local.drp_id}
  EOT
}

resource "google_compute_instance" "drp_server" {
  name         = "drp-server"
  zone         = "us-central1-a"
  machine_type = "n1-standard-1"
  tags         = ["digitalrebar"]
  boot_disk {
    initialize_params {
      image = "centos-stream-8-v20211105"
    }
  }
  metadata = {
    ssh-keys                   = "root:${chomp(file("~/.ssh/id_rsa.pub"))}"
    serial-port-logging-enable = "FALSE"
  }

  metadata_startup_script = local.cloud_init_script

  network_interface {
    network = google_compute_network.default.name
    access_config {
      // Ephemeral IP
    }
  }
}

resource "google_compute_firewall" "drp-server" {
  name          = "drp-server"
  network       = google_compute_network.default.name
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["22", "8090-8092"]
  }

  source_tags = ["digitalrebar"]
  target_tags = ["digitalrebar"]
}

resource "google_compute_network" "default" {
  name = "drp-network"
}

output "drp_credentials" {
  value       = "export RS_KEY=${var.drp_username}:${var.drp_password}"
  description = "export of DRP credentials"
}

output "drp_manager" {
  value       = "export RS_ENDPOINT=https://${google_compute_instance.drp_server.network_interface[0].access_config[0].nat_ip}:8092"
  description = "export of URL of DRP"
}
