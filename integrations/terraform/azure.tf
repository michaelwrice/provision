# Azure Quickstart
variable "drp_username" {
  type    = string
  default = "rocketskates"
}

variable "drp_password" {
  type    = string
  default = "r0cketsk8ts"
}

variable "drp_version" {
  type    = string
  default = "tip"
}

variable "drp_id" {
  type    = string
  default = "!default!"
}

variable "ssh_username" {
  type = string
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.10.0"
    }
  }
  required_version = ">= 1.0"
}

provider "azurerm" {
  features {}
}

locals {
  drp_id            = var.drp_id != "!default!" ? "--drp-id=${var.drp_id}" : ""
  cloud_init_script = <<-EOT
#!/bin/bash
value=$(curl -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2021-05-01&format=text")
curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --version=${var.drp_version} --ipaddr=$value --drp-password=${var.drp_password} --remove-rocketskates --drp-user=${var.drp_username} ${local.drp_id}
EOT
}

resource "azurerm_resource_group" "main" {
  name     = "digitalrebar-resources"
  location = "eastus"
}

resource "azurerm_virtual_network" "main" {
  name                = "digitalrebar-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.2.0/24"]
}


resource "azurerm_network_security_group" "security_group" {
  name                = "digitalrebar-security-group"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "nfp-0_"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "nfp-1_"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8090-8092"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

}


resource "azurerm_public_ip" "publicip" {
  name                = "digitalrebar-public-ip"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "machine_nic" {
  name                = "digitalrebar-nic"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip.id
  }
}

resource "azurerm_network_interface_security_group_association" "security_group_assn" {
  network_interface_id      = azurerm_network_interface.machine_nic.id
  network_security_group_id = azurerm_network_security_group.security_group.id
}

resource "azurerm_linux_virtual_machine" "drp_machine" {
  name                = "digitalrebar"
  computer_name       = "digitalrebar"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  size                = "Standard_D2s_v3"
  admin_username      = var.ssh_username
  network_interface_ids = [
    azurerm_network_interface.machine_nic.id
  ]

  admin_ssh_key {
    username   = var.ssh_username
    public_key = chomp(file("~/.ssh/id_rsa.pub"))
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  custom_data = base64encode(local.cloud_init_script)

  source_image_reference {
    publisher = "canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }

  tags = {
    Provisoner = "digitalrebar"
  }
}

output "drp_credentials" {
  value       = "export RS_KEY=${var.drp_username}:${var.drp_password}"
  description = "export of DRP credentials"
}

output "drp_manager" {
  value       = "export RS_ENDPOINT=https://${azurerm_linux_virtual_machine.drp_machine.public_ip_address}:8092"
  description = "export of URL of DRP"
}





