# Libvirt Quickstart

variable "drp_count" {
  type    = number
  default = 1
}

variable "drp_hostname" {
  type    = string
  default = "drp-server"
}

variable "drp_password" {
  type    = string
  default = "r0cketsk8ts"
}

variable "drp_username" {
  type    = string
  default = "rocketskates"
}

variable "drp_version" {
  type    = string
  default = "tip"
}

variable "libvirt_base_image" {
  type    = string
  default = ""
}

variable "libvirt_network" {
  type    = string
  default = "default"
}

variable "libvirt_pool" {
  type    = string
  default = "default"
}

variable "libvirt_uri" {
  type = string
}

terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "~> 0.7"
    }
  }
}

provider "libvirt" {
  uri = var.libvirt_uri
}

resource "libvirt_volume" "base_image" {
  count = var.libvirt_base_image == "" ? 1 : 0

  name   = "drp_base.qcow2"
  pool   = var.libvirt_pool
  source = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-disk-kvm.img"
}

resource "libvirt_volume" "drp_server" {
  count = var.drp_count

  base_volume_name = var.libvirt_base_image == "" ? libvirt_volume.base_image[0].name : var.libvirt_base_image
  name             = "${var.drp_hostname}${count.index + 1}.qcow2"
  pool             = var.libvirt_pool
  size             = 20 * 1024 * 1024 * 1024
}

resource "libvirt_cloudinit_disk" "drp_cloudinit" {
  count = var.drp_count

  name      = "drp_cloudinit${count.index + 1}.iso"
  meta_data = <<EOT
instance-id: ${count.index + 1}
local-hostname: ${var.drp_hostname}${count.index + 1}
EOT
  pool      = var.libvirt_pool
  user_data = <<EOT
#cloud-config

packages:
  - qemu-guest-agent
runcmd:
  - systemctl enable --now qemu-guest-agent
  - curl -fsSL get.rebar.digital/tip | sudo bash -s -- install --universal --version=${var.drp_version} --drp-password=${var.drp_password} --remove-rocketskates --drp-user=${var.drp_username} --drp-id=${var.drp_hostname}${count.index + 1}
ssh_authorized_keys:
  - ${chomp(file("~/.ssh/id_rsa.pub"))}
EOT
}

resource "libvirt_domain" "drp_server" {
  count = var.drp_count

  cloudinit  = libvirt_cloudinit_disk.drp_cloudinit[count.index].id
  memory     = 4096
  name       = "${var.drp_hostname}${count.index + 1}"
  qemu_agent = true
  vcpu       = 1

  disk {
    volume_id = libvirt_volume.drp_server[count.index].id
  }

  network_interface {
    hostname       = "${var.drp_hostname}${count.index + 1}"
    network_name   = var.libvirt_network
    wait_for_lease = true
  }
}
output "drp_credentials" {
  value       = "export RS_KEY=${var.drp_username}:${var.drp_password}"
  description = "export of DRP credentials"
}

output "drp_manager" {
  value       = "export RS_ENDPOINT=https://${libvirt_domain.drp_server[0].network_interface[0].addresses[0]}:8092"
  description = "export of URL of DRP"
}

