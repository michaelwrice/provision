.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Release v4.13
  pair: Digital Rebar Provision; Release Notes

.. _rs_release_v4_13:

Digital Rebar version 4.13 [in planning]
---------------------------------------

Release Date: Summer 2023

Release Themes: TBD

TBD Executive Summary

See :ref:`rs_release_summaries` for a complete list of all releases.

.. _rs_release_v4_13_notices:

Important Notices
~~~~~~~~~~~~~~~~~

New for this release:

* TBD

From prior releases:

* copy from prior

.. _rs_release_v4_13_vulns:

Vulnerabilities
+++++++++++++++

None known

.. _rs_release_v4_13_deprecations:

Deprecations
++++++++++++

None known

.. _rs_release_v4_13_removals:

Removals
++++++++

None known

FEATURES
~~~~~~~~

**No committed features at this time**

Work in Process Items

These items are under active development, but cannot be confirmed for delivery in this release.  Please consult with RackN if items below are mission critical for your operation.

TBD
+++

TBD

.. _rs_release_v4_13_otheritems:

General UX Improvements (applies to all DRP versions)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* TBD

Other Items of Note
~~~~~~~~~~~~~~~~~~~

* TBD

Roadmap Items (unprioritized, planned for future release)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* TBD