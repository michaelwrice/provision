.. Copyright (c) 2017 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Portal

.. _rs_portal:

RackN Portal
============

The RackN Portal provides a complete graphical solution to manage Digital Rebar Provision.

.. toctree::

   portal/homeux
   portal/systemux
   portal/networkingux
   portal/provisionux
   portal/controlux
   portal/syncuploadux
   portal/endpointadminux
   portal/configux
   portal/branded-portal

.. _rs_data_metadata_ux:

UX Metadata
===========

The following items are commonly used on all Object types to provide UX rendering guides:

* ``icon: [icon name]`` - sets the icon
* ``color: [color name]`` - set the color

The following items are commonly used on Params to provide UX rendering guides:

* ``password: [anyvalue]`` - renders as password (NOT encrypted, just obfuscated)
* ``clipboard: [anyvalue]`` - provides a copy button so user can copy the param contents to clipboard.
* ``readonly: [anyvalue]`` - does not allow UX editing
* ``render: link`` - adds an https://[value] link field that opens in a new tab.
* ``render: multiline`` - presents a textarea for string inputs
* ``render: raw`` - presents textarea to user instead of regular edit, does not reformat
* ``downloadable: [file name]`` - provides download link so user can download the param contents as a file.  Name of the file is the value of the Meta.downloadable.
* ``route: [object name]`` - when included, the UX will provide a link to the underlying object as per the Param value

A complete list of .Meta values are used throughtout Digital Rebar for both UX and operational controls.  Please consult :ref:`rs_data_metadata_params`.
