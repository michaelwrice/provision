.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license

.. REFERENCE kb-00000 for an example and information on how to use this template.
.. If you make EDITS - ensure you update footer release date information.
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _running_workflow_on_a_macos_system:
.. _rs_kb_00077:

kb-00077: Running Workflow on a MacOS System
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00077


Description
-----------

This article discusses a basic / barebones method to get a RackN
Digital Rebar Platform (DRP) Agent (or "Runner") up and running on a MacOS system
to process Tasks (via Pipelines, Workflows, Workorders, or Stages).

There are several technical issues that still need to be resolved around MacOS
as a supported platform for running DRP tasks.  The primary issues that are
unresolved by this article are:

- The *joinup* discovery script (``join-up.sh``) method will not work on MacOS systems
- MAC Addresses (Machine ``HardwareAddrs`` field) SHOULD be populated
- Machine IP Address (Machine ``Address`` field) SHOULD be populated
- The Machine object is created manually with basic information (Name, and optionally the HardwareAddrs" or "Addresses")
- Machine inventory (``gohai-inventory``) does not run on MacOS at this time

Note that MacOS platforms are generally NOT network installable as Apple
has locked down and removed most of all of the network installation paths
that DRP supports.

This article only discusses how to get a MacOS system up and processing
Tasks in a rudimentary fashion.

The DRP Agent (runner) will start on boot up of the MacOS system via this
mechanism.

Solution
--------

The overall process is relatively simple:

- Create a Machine object for management of the target MacOS system
- Add the ``drpcli`` binary to your target MacOS system
- Create a Machine Token to authorize the Machine Agent to connect to the DRP Endpoint service
- Collect the Machines UUID for the Agent configuration file
- Install the Agent configuration and startup scripts
- Start the DRP Agent (Runner) on the target MacOS system with the correct configuration

The last two steps (install Agent config/startup, and Start the DRP
Agent are completed via the ``drpcli`` binary itself.


**Create Machine Object**

To create the Machine object for management of the target MacOS system,
start with the following script.  You REALLY should modify the Machine
MAC Addresses and IP Address appropriately (see the commented out info
in the script).  While these are not absolutely necessary, they are
needed by a lot of tasks that expect them to exist.

These could potentially be added to the Machine object with a Task (via
Workflow mode) or Blueprint (via Workorder mode).

YOU MUST MODIFY THIS SCRIPT FOR YOUR ENVIRONMENT AND EACH MACHINE !!

  ::

    #!/usr/bin/env bash
    # Create machine object for management by DRP Endpoint

    #### UPDATE these to your DRP Endpoint information
    export RS_ENDPOINT="https://drp.example.com:8092"
    export RS_USERNAME="rocketskates"
    export RS_PASSWORD="r0cketsk8ts"

    #### NAME and UUID must be unique keys in all Machine Objects
    export NAME="fuji"
    export RS_UUID=$(uuidgen)

    set -e

    echo "Create initial Machine object"

    # You really SHOULD also set 'HardwareAddrs' (list of MAC
    # addresses) and 'Address' (IP address of Machine); example add:
    # HardwareAddrs:
    # - 11:22:33:44:55:66
    # - 22:33:44:55:66:77
    # Address: 10.10.10.11

    cat << EOCREATE | drpcli machines create - > /dev/null
    ---
    Name: $NAME
    Uuid: $RS_UUID
    EOCREATE

    echo "Update Machine workflow and bootenv"

    # !!!!!!
    # IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT
    # !!!!!!
    # we must update machine object to remove defaultWorkflow and
    # BootEnv setting values that are forced by Info & Preferences
    # these can not be set on initial create of the Machine object
    drpcli machines update Name:$NAME '{ "Workflow": "", "BootEnv": "local" }' > /dev/null

    # get a Machine Token to authorize Agent communication with
    # the DRP Endpoint service (you can adjust the TTL, but it
    # should be "long lived"
    echo "Generating a machine Token for authorization for Agent"
    export RS_TOKEN=$(drpcli machines token $RS_UUID -ttl=5y | jq -r '.Token')

    echo
    echo "Agent download URL for x86_64 (amd64):"
    echo "  https://$RS_ENDPOINT/files/drpcli.amd64.darwin"
    echo "or for ARM:"
    echo "  https://$RS_ENDPOINT/files/drpcli.arm64.darwin"

    echo "done"

Once the Machine object has been created, you should verify on the DRP Endpoint
that the Machine exists, is currently set to the "``local``" BootEnv, and there
is no Workflow or Stage set on the machine.  This ensures that once the Agent
is started, no unexpected changes will occur on the machine.

You may also choose to set the Agent/Runner to "Pause" mode as an additional
safety check.


**Get** ``drpcli`` **on Machine**

You must download the ``drpcli`` binary to the target Machine that is going to
be managed.  The CLI binary (``drpcli``) also doubles as the Agent and Runner,
when specific arguments are passed to it.

The above script should also output an appropriate URL for download of either
the amd64 (x86_64) or arm64 ``drpcli`` binary.  Ensure you use the correct
binary type for the target MacOS system.

Example:

  ::

    RS_ENDPOINT="https://drp.example.com:8092"
    MACH="amd64"
    #MACH="arm64"
    sudo wget --no-check-certificate https://$RS_ENDPOINT/files/drpcli.$MACH.darwin -O /usr/local/bin/drpcli
    sudo chmod 755 /usr/local/bin/drpcli

Verify that you can run the ``drpcli`` binary successfully:

  ::

    drpcli version

This assumes that ``/usr/local/bin`` is in the current users PATH.  This
should produce an output something like:

  ::

    Version: v4.9.0


**Install Agent Config and Startup Scripts**

Once you have the ``drpcli`` binary in place, you can install a config
with the correct RS_ENDPOINT, RS_UUID, and RS_TOKEN credentials in it
via the ``drpcli`` binary itself.

You MUST RUN the following scriptlet with ``root`` level permissions
on the MacOS system as systemwide configuration and startup processes
are modified.  NOTE that the ``RS_*`` variables are used by the Agent
Install process to setup the configuration file correctly.  THESE MUST
be correct!

  ::

    # must be run as root
    sudo su -

    # RS_ENDPOINT, RS_TOKEN, and RS_UUID are from the previous script output
    # above - these tell the Agent where to connect to, and allows it
    # to authenticate to the DRP Endpoint (via Token and UUID).
    RS_ENDPOINT=https://drp.example.com:8092
    RS_UUID=<GENERATED_IN_ABOVE_SCRTIPT>
    RS_TOKEN=<GENERATED_IN_ABOVE_SCRTIPT>

    drpcli agent install

The output of this should be the following files added to the system:

  * ``/var/lib/drp-agent/drp-agent.cfg``: contains the configuration with the
    RS_ENDPOINT, RS_UUID, and RS_TOKEN information for the DRP Agent to connect
    to the correct DRP Endpoint with authorizaiont
  * ``/Library/LaunchDaemons/drp-agent.plist``: The MacOS startup *plist*
    service file


**Start the Agent**

Now you will start an Agent on the target system via the ``drpcli`` binary
itself; which will source the  ``/var/lib/drp-agent/drp-agent.cfg`` file
and launch a background service based on that configuration.

.. warning:: It is important that the Machine Object has been updated as
             previously specified (no Workflow, Stages, Tasks, and the
             BootEnv set to ``local``), otherwise the system may attempt
             to execute Workflow and Reboot !!!!!

Example:

  ::

    sudo drpcli agent start

Service logs will be written to the configuration specified in the *plist*
startup file.  By default this would be:


  ::

    # found in /Library/LaunchDaemons/drp-agent.plist

    # standard output log:
    /usr/local/var/log/drp-agent.out.log

    # error output log:
    /usr/local/var/log/drp-agent.err.log


**Test the Agent**

Install the ``dev-library`` content pack on the DRP Endpoint if it is not already
installed.  See the *Catalog* in the Portal, or run:

  ::

    drpcli catalog item install dev-library

- Go to the *Machines* page on the Portal.
- Select the newly created Machine
- Set the Workflow to "``hello-world``"
- If you had set the Agent to paused, set it to Runnable now
- The "hello world" workflow should execute and run
- Check the Machine Object "activity" tab for the Job Log output

You should get output similar to the following:

  ::

    Command running

    >>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<<<<<

    Hello World - the date is:
    Fri Apr 22 16:10:08 PDT 2022
    This machines name is:
    fuji
    The 'hello world' message has been set to:
    Hello World

    >>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<<<<<

    Command exited with status 0


Additional Information
----------------------

This is only a fast example to show that it is possible to
run DRP Tasks on target MacOS systems.

This process is NOT a supported method for managing MacOS systems with in DRP.

Generally speaking, a Machine object should always contain HardwareAddrs and Address
fields with appropriate details.  These can be set on the Machine object at
creation time.  See the above referenced script comments.

Some standard RackN provided content (Tasks/Templates) may not correctly run
in the MacOS environment.  For example, the ``drpcli gohai`` command will not
work by default because DMI bios information/data is not available as standard
on MacOS systems.

Many tasks that identify the OS and Architecture may not correctly reflect
MacOS systems.  If you identify any of these areas, please contact RackN
Support and let us know.


See Also
========


Versions
========

All

Keywords
========

drpcli, macos, agent, runner, workflow, tasks, workorders, pipelines, apple

Revision Information
====================
  ::

    KB Article     :  kb-00077
    initial release:  Fri Apr 22 15:49:59 PDT 2022
    updated release:  Fri Apr 29 15:05:00 PDT 2022

