.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license

.. REFERENCE kb-00000 for an example and information on how to use this template.
.. If you make EDITS - ensure you update footer release date information.
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _icons_for_meta_icon_in_objects:
.. _rs_kb_00069:

kb-00069: Icons for 'Meta.icon' field in content objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00069


Description
-----------

This Knowledge Base article describes the set of Icons that are used by
the RackN Portal (UX) that are specified in Content objects (workflows,
stages, tasks, profiles, params, bootenvs, contexts, etc.).


Solution
--------

The RackN Portal (UX) uses the following Semantic UI Icon set internally:

  * https://react.semantic-ui.com/elements/icon/



Additional Information
----------------------

These Icons are used in Content object fields in the ``Meta`` section.

A partial example of a ``Meta`` section that uses the Icon and Color values:

  ::

    Meta:
      icon: motorcycle
      color: purple


See Also
========

  * :ref:`rs_data_architecture`

Versions
========

All Versions

Keywords
========

icons, color, meta, portal, ux

Revision Information
====================
  ::

    KB Article     :  kb-00069
    initial release:  Wed Nov  3 14:11:01 PDT 2021
    updated release:  Wed Nov  3 14:11:01 PDT 2021

