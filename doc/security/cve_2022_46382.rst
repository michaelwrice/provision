.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Security

.. _rs_cve_2022_46382:

CVE-2022-46382: Deleted User's Tokens Not Invalidated
=====================================================

Summary
-------

After signing into Digital Rebar, users are issued authentication tokens tied to their account to perform actions within Digital Rebar.  During the validation process of these tokens, Digital Rebar did not check if the user account still exists.  Deleted Digital Rebar users could still use their tokens to perform actions within Digital Rebar.

Technical Details
-----------------

Digital Rebar’s API supports various authentication methods for API requests: username and password, certificate, and bearer JWT token authentication.  The JWT token authentication method is a stateless form of authentication where the Digital Rebar server generates a cryptographically signed token that embeds user and session details, including expiration dates.

A token validation bug was discovered that enabled deleted user accounts to still use previously generated tokens until the token expired.  The user details embedded in the token were not being checked to see if the user was still a valid user.

Recommendations
---------------

A fix has been developed to ensure tokens are only used by valid user accounts.  Digital Rebar users should update to the latest fixed version.

Affected Versions
-----------------

=================  =============
Affected Versions  Fixed Version
=================  =============
v4.5 and earlier   **v4.6.15**
v4.6               **v4.6.15**
v4.7               **v4.7.23**
v4.8               **v4.8.6**
v4.9               **v4.9.13**
v4.10              **v4.10.9**
=================  =============

Common Vulnerability Scoring System (CVSS) Score
------------------------------------------------

==========================  =============
**CVSS Base Score**         **7.4**
**Attack Vector**           Network
**Attack Complexity**       Low
**Privileges Required**     Low
**User Interaction**        None
**Scope**                   Changed
**Confidentiality Impact**  Low
**Integrity Impact**        Low
**Availability Impact**     Low
==========================  =============

CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:L
