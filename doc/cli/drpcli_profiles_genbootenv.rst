
.. _rs_drpcli_profiles_genbootenv:

drpcli profiles genbootenv
--------------------------

Create bootenv profiles

Synopsis
~~~~~~~~

Create a bootenv profile or a bunch of bootenv profiles using a provided
CSV file:

Example of what the CSV file should look like:

Name,IsoFile,Sha256,BootEnv,Version,esxi/enable-legacy-install,vmware/esxi-version-override
esxi-702a-1787351-DU01,VMware-ESXi-7.0.2a-17867351-DU01.iso,69c00595d9feba5beb1006507e0eadcb15caaffa6bb167b79ee534700834062f,esxi-install,702,true,esxi-install
esxi-700u2-17630552,VMware-VMvisor-Installer-7.0U2-17630552.x86_64.iso,ff20603e4a3e75ab20c7752ca4e3e28d55d28730d6947c849a4cc5beacf9878d,esxi-install,702,true,esxi-install

The first 5 items are required and any additional item added is treated
as a param in the bootenv-overrides

Example usage might look like: drpcli profiles genbootenv –csvFile
/tmp/my_custom_file.csv –outputDir /tmp

::

   drpcli profiles genbootenv [flags]

Options
~~~~~~~

::

         --csvFile string     --csvFile /path/to/file
     -h, --help               help for genbootenv
         --outputDir string   --outputDir /path/to/outDir (default "/tmp")

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli profiles <drpcli_profiles.html>`__ - Access CLI commands
   relating to profiles
