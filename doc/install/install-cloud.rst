.. Copyright (c) 2017 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Install Cloud

.. _rs_install_cloud:

Cloud & VM Install (Non-PXE environments)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This Cloud install guide provides a multi-cloud management focused installation process that runs DRP inside a Cloud Provider using Resource Brokers.

Other installation paths:

* :ref:`rs_quickstart` is a basic `systemd <https://en.wikipedia.org/wiki/Systemd>`_ install for new users
* :ref:`rs_install` details more complex installs including offline/airgap.
* :ref:`rs_install_dev` for developers running DRP interactively
* :ref:`rs_install_airgap` for users installing on an airgapped system
* :ref:`rs_install_docker` for trial users minimizing their install requirements
* `Edge Lab with RPi <http://edgelab.digital>`_ is self-contained Digital Rebar inexpensive lab using Raspberry Pi computers.

Unlike other environments which requires careful setup up of your network environment and consideration with regard to competing DHCP services, this setup does not use DHCP or PXE provisioning.

You must install Digital Rebar to use it, there is no SaaS version.  :ref:`rs_self_managed_why`

.. _rs_cloud_preparation:

Preparation
-----------

Acquire a Linux Cloud or Virtual instance.  It should have at least 4 Gb or RAM and 20 Gb of storage available for a long term system.  Short term trials can use as little as 1 CPU and 1 Gb of RAM.

You *must* provide access to TCP/8092, TCP/8091 and TCP/8090 (default ports) to access Digital Rebar.  Openning ports varies depending on the provider.

.. _rs_cloud_quickstart:

Install via Terraform
---------------------

For users who are comfortable with Terraform, RackN maintains quickstart plans for popular clouds that automatically allocated and bootstrap Digital Rebar as per the Install process below.  While they should work "out of the box," Operations should review the plans and modify them to fit their environment.  If missing, plans will prompt for required values.

Precreated Terraform Plans:

* AWS Cloud: https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/aws.tf
* Azure: https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/azure.tf
* Google: https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/google.tf
* Digital Ocean: https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/digitalocean.tf
* Libvirt: https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/libvirt/libvirt.tf
* Linode: https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/linode.tf

This example shows how to use the above scripts with Terraform.  Remember that Terraform will complete the provisioning request before Digital Rebar is installed so it will take additional time for the installation to complete.

.. code-block:: bash

    # replace with the cloud provider TF file from above
    curl -o main.tf https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/aws.tf
    terraform init
    terraform apply -var="drp_username=rackn" -var="drp_password=myHardP4ssword"
  
When using these plans, we strongly recommend passing values as shown to override the default username and password variables `drp_username` and `drp_password` to override the default username and password during installation.  For example: `-var drp_user="rackn" -var drp_password="myHardP4ssword"`.  You can also set drp_version and drp_id via Terraform variables.

When Terraform completes, it outputs the identity and URL information.  Please note that it will take additional time for the install to complete.  Since the API is available during the self-bootstrap, users are able login during that phase of the installation and observe the processes.

.. _rs_cloud_install:

Install via manual create
-------------------------

To begin, execute the following commands in an SSH session or during instance cloud-init process:

The example is for AWS clouds because using it's API to get the public IP address for Digital Rebar.  For other clouds,
replace the API call with the one for that platform or manually provide the public IP.

.. code-block:: bash

    #!/usr/bin/env bash
    # AWS specific API call
    value=$(curl -sfL http://169.254.169.254/latest/meta-data/public-ipv4)
    # Google: value=$(curl -sfL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)
    # Azure: value=$(curl -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2021-05-01&format=text")
    # Digital Ocean: value=$(curl -sfL http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
    curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --version=tip --ipaddr=$value

The command will download the tip Digital Rebar (the ``systemctl`` service name is ``dr-provision``) bundle and checksum from github, extract the files, verify prerequisites are installed, and create needed directories and links under ``/var/lib/dr-provision``.  The ``--systemd`` and ``--version`` flags included for clarity, they are not required for this install.

The `install <http://get.rebar.digital/stable/>`_ script used by our installs has many additional options including ``remove`` that are documented in its help and explored in other install guides.

Once the installation script completes, a Digital Rebar endpoint will be running your instance!

Follow the steps in :ref:`rs_qs_ux_bootstrap` to registered your Digital Rebar endpoint.

.. _rs_cloud_provisioning:

Clusters and Brokers
--------------------

These instructions apply to v4.8 and later.

Universal installation automatically build the correct container infratructure for cloud clusters and brokers.

Create a resource broker using one of the cloud resource profiles.  The UX wizard will
assist you in selecting a cloud resource profile and adding the required configuration
parameters.

Create a cluster via the UX cluster wizard and select your broker.  The system will automatically create the
machines to back the cluster using a generated Terraform plan based on your broker
inputs.

.. _rs_cloud_cleanup:

Clean Up
--------

Once you are finished exploring Digital Rebar Provision in the cloud, cleanup any clusters
that you created (that will remove the created VMs) and then remove the instance.
