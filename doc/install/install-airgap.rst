.. Copyright (c) 2017 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Install Airgap

.. _rs_install_airgap:

Airgap Install
~~~~~~~~~~~~~~

In an air-gapped install, the DRP endpoint will not have access to the internet. In order to have a functional DRP endpoint, you will
need to pre-fetch some packages from the internet. This document will cover what the minimal requirements will be to get up and running
in the air gapped environment. We will cover what is needed and where to get it. In this document, it will be assumed you will be using
RHEL/CentOS 7 and starting the install of DRP after you have completed at least a "Minimal install" of the operating system. The
commands being run to pre-fetch content are being run from an Ubuntu workstation machine with internet access, a web browser, and wget,
but any workstation can be used as long as it has internet access and a way to download and save files from the internet.

Other installation paths:

* :ref:`rs_quickstart` is a basic `systemd <https://en.wikipedia.org/wiki/Systemd>`_ install for new users
* :ref:`rs_install` details more complex installs including offline/airgap.
* :ref:`rs_install_dev` for developers running DRP interactively
* :ref:`rs_install_cloud` is non-PXE / Cloud-Only installation process (no DHCP required)
* :ref:`rs_install_docker` for trial users minimizing their install requirements
* `Edge Lab with RPi <http://edgelab.digital>`_ is self-contained Digital Rebar inexpensive lab using Raspberry Pi computers.

Preparation
-----------

The following packages will need to be installed on your air gapped endpoint:

* curl

Step By Step
------------

The following commands should be run on the internet accessible workstation. If you do not have access to a UX you will also need to follow instructions to install UX locally.

* Build the airgap-install.sh script by running the following:

  ::

    curl -fsSL get.rebar.digital/stable | bash -s build-airgap <args for airgap>
    # example: curl -fsSL get.rebar.digital/stable | bash -s build-airgap --location=dir_name --version=stable --concurrency=16

This will generate a self extracting script called `airgap-install.sh` in the location specified.
Note:
If you also require a local UX on your airgapped system, please contact RackN to get the UX catalog.
You will need to provide the updated catalog to `build-airgap` like so:

  ::

    curl -fsSL get.rebar.digital/stable | bash -s build-airgap --download-iso-bootenvs="sledgehammer, discovery" --download-iso-profiles="sledgehammer,discovery" --catalog="ux-catalog" ...<other args>

For a complete list of build-airgap args see :ref:`rs_drpcli_airgap`

* Once your airgap-install.sh is built, you will need to move it over to your airgapped system and run the following:

  ::

    ./airgap-install.sh install --universal
    # this will install drp in /var/lib/dr-provision by default
    # a different location can be specified using the --drp-home-dir flag
    # all the flags you would normally use with your regular install script can be used here
    # if you also want to run UX locally and have built the script with a UX catalog, you can run
    ./airgap-install.sh install --universal --local-ui


.. note::

  Please note that you also have the option to just run:

    ::

      ./airgap-install.sh explode
      # This will only explode the catalog to the current dir. A location can also be specified
      ./airgap-install.sh extract
      # This will only extract the tarball to the current dir or location specified


* Now to verify the portal is working. On a machine that can access the air gapped endpoint open a web browser and
  visit `https://<YOUR IP>:8092/`. By default, a self signed cert will need to be accepted (Note that you can provide
  your own certs during deployment.) Log in using the default user and password. Once logged in, you will be prompted
  to upload a valid license.

.. note:: In addition to port 8092, a self signed cert will need to be accepted by visiting `https://<YOUR IP>:8090/` so that the local catalog can be displayed in the UI.

* At this time you, need to either generate the license from an existing internet accessible DRP endpoint,
  or by contacting RackN support to have a license generated for you. Once you have a valid license you can upload it from the UX.

* Once you have uploaded your license, please verify that your self runner has started up successfully. You can do this by going to the
  machines tab. You should only see one machine and it should show up with a green play button next to it. If something has gone wrong during
  the initial bootstrap process, you should be able to look at logs to figure out what went wrong, fix it and restart the workflow.
