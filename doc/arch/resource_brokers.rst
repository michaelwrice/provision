.. Copyright (c) 2021 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Resource Brokers

.. _rs_resource_brokers:

Resource Brokers
================

Digital Rebar multi-machine clustering patterns were updated to leverage resource brokers in v4.8.

This documentation is under development.

See also: :ref:`rs_universal_broker`

