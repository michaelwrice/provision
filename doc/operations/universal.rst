.. Copyright (c) 2020 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Platform documentation under Digital Rebar master license
..
.. doc hierarchy: ~ - =
.. index::
  pair: Digital Rebar Platform; Universal Workflow Operations

.. _rs_universal_ops:

Universal Workflow Operations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section will address usage of the Universal Workflow system.  The architecture and implementation of the Universal
Workflow system is described at :ref:`rs_universal_arch`.

.. _rs_universal_actions:

Actions
-------

.. _rs_universal_apply_application:

Applying an Application
=======================

.. _rs_universal_build_application:

Building an Application
=======================

.. _rs_universal_classification:

Modifying Discovery Classifier
==============================

.. _rs_universal_flexiflow:

Adding Tasks to a Workflow
==========================

.. _rs_universal_callback:

Configuring Callbacks
=====================

.. _rs_universal_validation:

Using Validations
=================


.. _rs_universal_workflows:

Provided Workflows
------------------

The following workflows are the default set of universal workflows.  Using these are starting points that do most default
operations needed to operate a data center.

Each workflow defines what it requires on start and set on completion.  These actions allow for the workflows to work
together.  For example, Discover should set the needed parameters for driving Hardware and the networking in the other workflows.
These are the elements defined by the universal parts of these components.

Universal depends upon:

  * Digital Rebar Community Content Pack
  * Task Library Content Pack
  * FlexiFlow Content Pack
  * Validation Content Pack
  * Callback Plugin
  * Classification Content Pack

Core Universal Workflows are detailed below.

.. _rs_universal_discover:

Discover
========

This workflow handles discovery a machine and inventories the machine.  It also will validate the basic initial pieces.
The final piece is classification to drive additional workflows if needed.

NOTE: Discover assumes that it is running in sledgehammer.

The default stages for `discover` are:

  * discover
  * inventory-minimal
  * centos-setup-repos
  * ipmi-inventory
  * bios-inventory
  * raid-inventory
  * network-lldp
  * inventory

A common set of post `discover` flexiflow tasks would be

  * rack-discover
  * universal-classify
  * universal-host-crt-callback

.. _rs_universal_start:

Start
=====

This workflow handles starting a virtual machine or context that does not need
a full bare metal discovery (see :ref:`rs_universal_discover`).  It also will
validate the basic initial pieces. The final piece is classification to drive additional workflows if needed.

NOTE: Unlike Discover, Start does not assumes that is running in sledgehammer.

The default stages for `start` are:

  * start
  * drp-agent
  * inventory-minimal
  * cloud-inventory
  * network-lldp
  * inventory

.. _rs_universal_hardware:

Hardware IPMI, Flash, RAID and BIOS Configure
=============================================

This workflow is used to configure hardware and burnin that state.  It is also the place where BMC certificates are loaded.

The workflow assumes that the parameters of configuration have been set.  See :ref:`rs_universal_classification` and :ref:`rs_universal_baseline`.

.. _rs_universal_baseline:

Build Baseline
==============

This workflow is used outside of a universal workflow to snapshot the hardware state of a machine and builds profiles
named such they work with the universal-classify stage/task.

.. _rs_universal_esxi:

ESXi Kickstart, Install, Config
===============================

Installs ESXi through multiple steps.

.. _rs_universal_solidfire:

Solidfire RTFI
==============

Handles updating and setting up SolidFire systems through RTFI process

.. _rs_universal_linux_install:

Linux Install
=============

Handles installing a linux operating system via network boot (kickstart or preseed).

Alternative Provisioning process in which disk images are written directly to media
instead of being installed via kickstart or preseed.

.. _rs_universal_linux_join:

Linux Join
==========

Runs standard install processes on Linux systems that are being added to Digital Rebar but where installed by another system (like a Cloud).

.. _rs_universal_image_deploy:

Image Deploy
============


Image Deploy - Handles deploying an image-based system

.. _rs_universal_image_capture:

Image Capture
=============

This is not a universal workflow in itself.  By altering the linux-install workflow with the following tasks....

this goes in the linux post install pre reboot.

  - image-reset-package-repos
  - image-update-packages
  - image-install-cloud-init
  - image-builder-cleanup
  - image-capture

.. _rs_universal_bootstrap:

Digital Rebar Bootstrap
=======================

This pipeline is used to bootstrap Digital Rebar Servers.

.. _rs_universal_runbook:

Runbook Processing
==================

Provides configuration management operations (e.g.: Ansible, Chef, Puppet, Salt) for installed machines.

.. _rs_universal_cluster_provision:

Cluster Provision
=================

Uses Cluster Managers (v4.8+) to manage clusters of machines.

.. _rs_universal_resource_broker:

Resource Broker Provision
=========================

Uses Resource Brokers (v4.8+) to provision and decommission cloud and virtual infratructure.

.. _rs_universal_burnin:

Burnin
======

Stress testing for system verification and validation before production deployment.

.. _rs_universal_decomission:

Decomission
===========

Decomission is used to clear, wipe and otherwise reset machines that are being removed from service.

.. _rs_universal_rebuild:

Rebuild
=======

Redrives the system through the process as a reset operation.

.. _rs_universal_decommission:

Decommission
============

Handles decommissioning hardware by removing data from systems and cleans them up for reuse or disposal.


.. _rs_universal_maintenance:

Maintenance
===========

Sets the system to go into non-destructive Hardware and returnes toe LocalIdle

.. _rs_universal_local:

Local
=====
Starts the existing O/S installed on the system without image or network provisioning.


.. _rs_universal_sledgehammer_wait:

SledgehammerWait
================

SledgehammerWait - End state for waiting or debugging

