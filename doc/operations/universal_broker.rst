.. Copyright (c) 2021 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Platform documentation under Digital Rebar master license
..
.. doc hierarchy: ~ - =
.. index::
  pair: Digital Rebar Platform; Universal Workflow Cluster

.. _rs_universal_broker:

Universal Resource Broker Operations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section will address usage of the Universal Workflow system for Resource Brokers (v4.8+).  The architecture and implementation of the Universal
Workflow system is described at :ref:`rs_universal_arch`.

Note: the cluster feature is specific to v4.8+

.. _rs_universal_brokers:

Brokers
-------

Resource Brokers provide a critical abstraction for handling machine lifecycles in a general
purpose away.  Automation, such as a Cluster workflow, can make a non-differentiated request
to a Resource Broker (generally via a work order) to allocate or release machines.  Since
the implementation is left to the broker, Digital Rebar operators are able to manage the
details of how and where the resources are provided.  For example, brokers can be used to
abstract different clouds or cloud regions.  They can also be used to front pools of physical
machines to allow cloud-like allocation for bare metal infrastructure.

At this time, there is no universal workflow applications for Resource Brokers