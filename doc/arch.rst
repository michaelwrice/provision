.. Copyright (c) 2017 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Architecture Reference

.. _rs_arch:

Architecture Reference
----------------------

This section is intended to act as a reference to the internals of
Digital Rebar for developers and power users.

.. toctree::

   arch/design
   arch/data
   arch/models
   arch/metadata
   arch/auth
   arch/provision
   arch/whoami
   arch/dhcp
   arch/sledgehammer
   arch/universal
   arch/hardware
   arch/workflow
   arch/workorder
   arch/runner-state
   arch/content-package
   arch/cluster
   arch/resource_brokers
   arch/pooling
   arch/pipeline
   arch/manager
   arch/Swagger
